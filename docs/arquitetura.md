# [README.md](../README.md)

- DeepSolar++: Understanding Residential Solar Adoption Trajectories with Computer Vision and Technology Diffusion Model

A deep learning framework to detect solar PV installations from historical satellite/aerial images and predict the installation year of PV. The model is applied to different places across the U.S. for uncovering solar adoption trajectories across time. The heterogeneity in solar adoption trajectories is further analyzed from the perspective of technology diffusion model.

To use the code, please cite the [paper](https://www.cell.com/joule/pdf/S2542-4351(22)00477-9.pdf):

* Wang, Z., Arlt, M. L., Zanocco, C., Majumdar, A., & Rajagopal, R. (2022). DeepSolar++: Understanding residential solar adoption trajectories with computer vision and technology diffusion models. Joule, 6(11), 2611-2625. DOI: https://doi.org/10.1016/j.joule.2022.09.011

The operating system for developing this code repo is Ubuntu 16.04, but it should also be able to run in other environments. The Python version used for developing this code repo is Python 3.6.

## Public data access

The census-block-group level time-series residential solar installation dataset can be accessed here:

* 420 counties (which are analyzed in the [DeepSolar++](https://www.cell.com/joule/pdf/S2542-4351(22)00477-9.pdf) paper): [CSV file data downloading link](https://opendatasharing.s3.us-west-2.amazonaws.com/DeepSolar2/data/residential_solar_installations_panel_data_420counties.csv)

* All counties in the contiguous US: [CSV file data downloading link](https://opendatasharing.s3.us-west-2.amazonaws.com/DeepSolar2/data/residential_solar_installations_panel_data_contiguous_US.csv).

In the CSV file, each census block group is corresponding to a row identified by its FIPS code (column 'blockgroup_FIPS'). Foe each block group, the table contains its cumulative number of residential PV installations in each year from 2005 to 2017, average GHI, building count, and ACS demographic data.

For the installation information from 2017 to 2022, we are still working on updating the data using the same methodology and will release it at some time point in 2023. 

## Install required packages

Run the following command line:

```
$ pip install -r requirements.txt
```

**Note**: multivariate OLS and logit regressions are run by the R code blocks inserted in the Python Jupyter Notework `bass_model_parameter_regression.ipynb`. It is based on the `rpy2` package. If you want to run these regressions, R and R kernel are required to install. Moreover, `lmtest` and `sandwich` are required libraries for R (which may need to be installed). 
For further details about installing R and R kernel, see this [tutorial](https://linuxize.com/post/how-to-install-r-on-ubuntu-20-04/) and this [tutorial](https://datatofish.com/r-jupyter-notebook/). For further details about using R in Python notebook, see [this](https://stackoverflow.com/questions/39008069/r-and-python-in-one-jupyter-notebook).

## Download data and model checkpoints

Run the following command lines to download the ZIP files right under the code repo directory:

```
$ curl -O https://opendatasharing.s3.us-west-2.amazonaws.com/DeepSolar2/checkpoint.zip
$ curl -O https://opendatasharing.s3.us-west-2.amazonaws.com/DeepSolar2/data.zip
$ curl -O https://opendatasharing.s3.us-west-2.amazonaws.com/DeepSolar2/results.zip
```

Unzip them such that the directory structure looks like:

```
DeepSolar_timelapse/checkpoint/...
DeepSolar_timelapse/data/...
DeepSolar_timelapse/results/...
```

**Note 1**: for the satellite/aerial imagery datasets under `data` directory (subdirectory `HR_images` for high-resolution (HR) images, `LR_images` for low-resolution (LR) images,  `blur_detection_images` for blur detection images, and `sequences` for image sequences), due to the restriction of imagery data sources, we are not able to publicly share the full data. Instead, for each subset (training/validation/test) and each class (e.g., positive/negative), we share two example images as a demo. For the image sequence dataset (`sequences`), we share one demo sequence (`sequences/demo_sequences/1`). Each image sequence contains satellite/aerial images captured in different years at the same location of a solar installation (image file name examples: `2006_0.png`, `2007_0.png`, `2007_1.png`, `2008_0.png`, etc). Users can put their own data under these directories.

**Note 2**: to run Jupyter Notebook, the default kernel/environment is "conda_tensorflow_p36", which does not necessarily exist in your computer. Please change the kernel to the one where all required packages are installed.

## Functionality of each script/notebook

### Part 1: model training with hyperparameter search

An image is first classfied by the blur detection model into one of the three classes according to its resolution: high resolution (HR), low resolution (LR), and extreme blurred/out of distribution (OOD). An OOD image is not used for determining the solar installation year; a HR image is classified by a single-branch CNN into two classes: positive (containing solar PV) and negative (otherwise); a LR image is classified by a two-branch Siamese CNN into two classes: positive (containing solar PV) and negative (otherwise). 

For training the blur detection model with hyperparameter search:
```
$ python hp_search_ood_multilabels.py
```
For training the HR model with hyperparameter search:
```
$ python hp_search_HR.py
```
For training the LR model with hyperparameter search:
```
$ python hp_search_LR_rgb.py
```

By default, all three scripts above are run on a machine with GPU.

### Part 2: deploying models to predict installation year

For each solar installation, we can retrieve a sequence of images captured in different years at its location and put them in the same folder. The images are named as `{image_capture_year}_{auxiliary_index}.png`. For example, if there are three images captured in 2012, they are named as `2012_0.png`, `2012_1.png`, and `2012_2.png`, respectively. 

For each image sequence, we deploy the blur detection model, HR model, and LR model. Their model outputs are combined together predict the installation year of the solar PV system.

First, we deploy the blur detection model and HR model to image sequences:
```
$ python predict_HR.py
```
```
$ python predict_ood_multilabels.py
```

Combining the prediction results of the above two models, we can generate the "anchor_images_dict" that maps a target image in a sequence to all its reference images in this sequence. This needs to be run before deploying the LR model, as the LR model needs to take a target image and one of its corresponding reference image as inputs. To do this, run the code blocks in this Jupyter Notebook:

```
generate_anchor_image_dict.ipynb
```

Then, deploy the LR model to image sequences:
```
$ python predict_LR_rgb.py
```

By default, all three `.py` scripts above are run on a machine with GPU.

Finally, run the code blocks in this Jupyter Notebook that combines all model prediction outputs to predict the installation year for each solar PV system:

```
predict_installation_year_from_image_sequences.ipynb
```

### Part 3: analyzing solar adoption trajectories across time using Bass model

By predicting the installation year of each system, we are able to obtain the number of installations in each year in each place. We provide such solar installation time-series dataframe at the census block group level covering the randomly-sampled 420 counties in ``results``. This dataframe is used as inputs for the following analysis. 

For the solar adoption trajectory in each block group, we use a classical technology adoption model, [Bass model](https://pubsonline.informs.org/doi/abs/10.1287/mnsc.15.5.215?casa_token=PXhDNyJRVhgAAAAA:ZbFnu9tpKcAJoUDE6JlpMyWvaaa0hyXeuFA2Edbg8EORBlPTUVHBWShq6c1yuA5SBaPBRyLCW1Q), to parameterize its shape. First, the Bass model fitting based on Non-linear least squares (NLS):

```
$ python bass_model_fit_adoption_curves.py
```

Then we can use two Jupyter Notebook to analyze the Bass model parameters that have been fitted. They can be run without running the Bass model fitting code as the intermediate result of model fitting has already been provided in `results`:

`bass_model_parameter_phase_analysis.ipynb`: Based on the fitted Bass model parameters, segment each solar adoption trajectory into four phases: pre-diffusion, ramp-up, ramp-down, and saturation, and analyze the fractions of block groups in each phase.

`bass_model_parameter_regression.ipynb`: Run multivariate regressions with various Bass model parameters as dependent variables and socioconomic characteristics (demographics, PV benefit, the presence of incentives, etc) as independent variables. R code blocks are inserted for running these regressions.

# [Titpredict_ood_multilabels.pyle](../predict_ood_multilabels.py)

from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets, models, transforms, utils
import torchvision.transforms.functional as TF

from tqdm import tqdm
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
# import skimage
# import skimage.io
# import skimage.transform
from PIL import Image
import time
import os
from os.path import join, exists
import copy
import random
from collections import OrderedDict
from sklearn.metrics import r2_score

from torch.nn import functional as F
from torchvision.models import Inception3, resnet18, resnet34, resnet50

from utils.image_dataset import *
from LR_models.siamese_model_rgb import *

"""
This script is for generating the prediction scores of blur detection model for images in 
sequences. A sequence of images are stored in a folder. An image is named by the year of 
the image plus an auxiliary index. E.g., '2007_0.png', '2007_1.png', '2008_0.png'.
"""

dir_list = ['demo_sequences']

root_data_dir = 'data/sequences'
old_ckpt_path = 'checkpoint/ood_ib1_0.2_decay_10_wd_0_22_last.tar'
result_path = 'results/ood_prob_dict.pickle'
error_list_path = 'results/ood_error_list.pickle'
model_arch = 'resnet50'

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
input_size = 299
batch_size = 64


class MyCrop:
    def __init__(self, top, left, height, width):
        self.top = top
        self.left = left
        self.height = height
        self.width = width

    def __call__(self, img):
        return TF.crop(img, self.top, self.left, self.height, self.width)


class SingleImageDatasetModified(Dataset):
    def __init__(self, dir_list, transform, latest_prob_dict):
        self.path_list = []
        self.transform = transform

        for subdir in dir_list:
            data_dir = join(root_data_dir, subdir)
            for folder in os.listdir(data_dir):
                idx = folder.split('_')[0]
                folder_dir = join(data_dir, folder)
                for f in os.listdir(folder_dir):
                    if not f[-4:] == '.png':
                        continue
                    if idx in latest_prob_dict and f in latest_prob_dict[idx]:
                        continue
                    self.path_list.append((subdir, folder, f))

    def __len__(self):
        return len(self.path_list)

    def __getitem__(self, index):
        subdir, folder, fname = self.path_list[index]
        image_path = join(root_data_dir, subdir, folder, fname)
        idx = folder.split('_')[0]
        img = Image.open(image_path)
        if not img.mode == 'RGB':
            img = img.convert('RGB')
        img = self.transform(img)
        return img, idx, fname


transform_test = transforms.Compose([
        transforms.Resize((input_size, input_size)),
        MyCrop(17, 0, 240, 299),
        # transforms.Resize((input_size, input_size)),
        transforms.ToTensor(),
        # transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ])


if __name__ == '__main__':
    # load existing prob dict or initialize a new one
    if exists(result_path):
        with open(result_path, 'rb') as f:
            prob_dict = pickle.load(f)
    else:
        prob_dict = {}

    # load existing error list or initialize a new one
    if exists(error_list_path):
        with open(error_list_path, 'rb') as f:
            error_list = pickle.load(f)
    else:
        error_list = []

    # dataloader
    dataset_pred = SingleImageDatasetModified(dir_list, transform=transform_test, latest_prob_dict=prob_dict)
    print('Dataset size: ' + str(len(dataset_pred)))
    dataloader_pred = DataLoader(dataset_pred, batch_size=batch_size, shuffle=False, num_workers=4)

    # model
    if model_arch == 'resnet18':
        model = resnet18(num_classes=2)
    elif model_arch == 'resnet34':
        model = resnet34(num_classes=2)
    elif model_arch == 'resnet50':
        model = resnet50(num_classes=2)
    elif model_arch == 'inception':
        model = Inception3(num_classes=2, aux_logits=True, transform_input=False)
    else:
        raise
    model = model.to(device)

    # load old parameters
    checkpoint = torch.load(old_ckpt_path, map_location=device)
    if old_ckpt_path[-4:] == '.tar':  # it is a checkpoint dictionary rather than just model parameters
        model.load_state_dict(checkpoint['model_state_dict'])
    else:
        model.load_state_dict(checkpoint)
    print('Old checkpoint loaded: ' + old_ckpt_path)

    model.eval()
    # run
    count = 0
    for inputs, idx_list, fname_list in tqdm(dataloader_pred):
        try:
            inputs = inputs.to(device)
            with torch.set_grad_enabled(False):
                outputs = model(inputs)
                prob = torch.sigmoid(outputs)
            prob_list = prob.cpu().numpy()
            for i in range(len(idx_list)):
                idx = idx_list[i]
                fname = fname_list[i]
                prob_sample = prob_list[i]

                if not idx in prob_dict:
                    prob_dict[idx] = {}
                prob_dict[idx][fname] = prob_sample

        except:  # take a note on the batch that causes error
            error_list.append((idx_list, fname_list))
        if count % 200 == 0:
            with open(join(result_path), 'wb') as f:
                pickle.dump(prob_dict, f)
            with open(join(error_list_path), 'wb') as f:
                pickle.dump(error_list, f)
        count += 1

    with open(join(result_path), 'wb') as f:
        pickle.dump(prob_dict, f)
    with open(join(error_list_path), 'wb') as f:
        pickle.dump(error_list, f)

    print('Done!')


[pypredict_LR_rgb.py](../.pypredict_LR_rgb.py)

from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets, models, transforms, utils
import torchvision.transforms.functional as TF

from tqdm import tqdm
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
# import skimage
# import skimage.io
# import skimage.transform
from PIL import Image
import time
import os
from os.path import join, exists
import copy
import random
from collections import OrderedDict
from sklearn.metrics import r2_score

from torch.nn import functional as F
from torchvision.models import Inception3

from utils.image_dataset import *
from LR_models.siamese_model_rgb import *

"""
This script is for generating the prediction scores of LR model for images in sequences.
A sequence of images are stored in a folder. An image is named by the year of the image 
plus an auxiliary index. E.g., '2007_0.png', '2007_1.png', '2008_0.png'.
"""

dir_list = ['demo_sequences']

root_data_dir = 'data/sequences'
old_ckpt_path = 'checkpoint/LR_nconvs_3_depth_128_nfilters_512_33_last.tar'
result_path = 'results/LR_prob_dict.pickle'
error_list_path = 'results/LR_error_list.pickle'
anchor_images_dict_path = 'results/anchor_images_dict.pickle'

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
backbone = 'resnet34'
input_size = 299
batch_size = 64

transform_test = transforms.Compose([
        transforms.Resize((input_size, input_size)),
        # transforms.Lambda(mask_image_info),
        transforms.ToTensor()
    ])


class ImagePairDatasetModified(Dataset):
    def __init__(self, dir_list, anchor_images_dict, transform, latest_prob_dict):
        self.couple_list = []
        self.transform = transform

        for subdir in dir_list:
            data_dir = join(root_data_dir, subdir)
            for folder in os.listdir(data_dir):
                idx = folder.split('_')[0]
                folder_dir = join(data_dir, folder)
                if idx not in anchor_images_dict:
                    continue
                anchor_images = anchor_images_dict[idx]
                for anchor_f in anchor_images:
                    for tar_f in os.listdir(folder_dir):
                        if not tar_f[-4:] == '.png':
                            continue
                        if idx in latest_prob_dict and anchor_f in latest_prob_dict[idx] and tar_f in latest_prob_dict[idx][anchor_f]:
                            continue
                        self.couple_list.append((subdir, folder, anchor_f, tar_f))

    def __len__(self):
        return len(self.couple_list)

    def __getitem__(self, index):
        subdir, folder, anchor_f, tar_f = self.couple_list[index]
        ref_img_path = join(root_data_dir, subdir, folder, anchor_f)
        tar_img_path = join(root_data_dir, subdir, folder, tar_f)
        idx = folder.split('_')[0]

        img_ref = Image.open(ref_img_path)
        img_tar = Image.open(tar_img_path)
        if not img_ref.mode == 'RGB':
            img_ref = img_ref.convert('RGB')
        if not img_tar.mode == 'RGB':
            img_tar = img_tar.convert('RGB')

        img_ref = self.transform(img_ref)
        img_tar = self.transform(img_tar)

        return img_ref, img_tar, idx, anchor_f, tar_f


if __name__ == '__main__':
    # load anchor_images_dict
    with open(anchor_images_dict_path, 'rb') as f:
        anchor_images_dict = pickle.load(f)

    # load existing prob dict or initialize a new one
    if exists(result_path):
        with open(result_path, 'rb') as f:
            prob_dict = pickle.load(f)
    else:
        prob_dict = {}

    # load existing error list or initialize a new one
    if exists(error_list_path):
        with open(error_list_path, 'rb') as f:
            error_list = pickle.load(f)
    else:
        error_list = []

    # dataloader
    dataset_pred = ImagePairDatasetModified(dir_list, anchor_images_dict, transform=transform_test, latest_prob_dict=prob_dict)
    print('Dataset size: ' + str(len(dataset_pred)))
    dataloader_pred = DataLoader(dataset_pred, batch_size=batch_size, shuffle=False, num_workers=4)

    # model
    model = psn_depthwise_cc_layerwise_3layers_l234(backbone=backbone, nconvs=3, depth=128, nfilters=512, kernel_size=3)
    model = model.to(device)

    # load old parameters
    checkpoint = torch.load(old_ckpt_path, map_location=device)
    if old_ckpt_path[-4:] == '.tar':  # it is a checkpoint dictionary rather than just model parameters
        model.load_state_dict(checkpoint['model_state_dict'])
    else:
        model.load_state_dict(checkpoint)
    print('Old checkpoint loaded: ' + old_ckpt_path)

    model.eval()

    # run
    count = 0
    for inputs_ref, inputs_tar, idx_list, anchor_f_list, tar_f_list in tqdm(dataloader_pred):
        try:
            inputs_ref = inputs_ref.to(device)
            inputs_tar = inputs_tar.to(device)
            with torch.set_grad_enabled(False):
                outputs = model(inputs_tar, inputs_ref)
                prob = F.softmax(outputs, dim=1)
            pos_prob_list = prob[:, 1].cpu().numpy()
            for i in range(len(idx_list)):
                idx = idx_list[i]
                anchor_f = anchor_f_list[i]
                tar_f = tar_f_list[i]
                pos_prob = pos_prob_list[i]

                if not idx in prob_dict:
                    prob_dict[idx] = {}
                if not anchor_f in prob_dict[idx]:
                    prob_dict[idx][anchor_f] = {}

                prob_dict[idx][anchor_f][tar_f] = pos_prob

        except:  # take a note on the batch that causes error
            error_list.append((idx_list, anchor_f_list, tar_f_list))

        if count % 400 == 0:
            with open(result_path, 'wb') as f:
                pickle.dump(prob_dict, f)
            with open(error_list_path, 'wb') as f:
                pickle.dump(error_list, f)
        count += 1

    with open(result_path, 'wb') as f:
        pickle.dump(prob_dict, f)
    with open(error_list_path, 'wb') as f:
        pickle.dump(error_list, f)

    print('Done!')

# [predict_installation_year_from_image_sequences.ipynb](../predict_installation_year_from_image_sequences.ipynb)

{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import os.path\n",
    "import pickle\n",
    "import shutil\n",
    "import pandas as pd\n",
    "import random\n",
    "import math\n",
    "import matplotlib.pyplot as plt\n",
    "from os.path import join, exists\n",
    "import numpy as np\n",
    "from tqdm import tqdm\n",
    "import random\n",
    "import copy\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook takes the prediction scores of HR model, LR model, and blur detection model (ood model) as inputs, and outputs the installation year prediction for each image sequence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir_list = ['demo_sequences']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "root_data_dir = 'data/sequences'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Load prob dicts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('results/HR_prob_dict.pickle', 'rb') as f:\n",
    "    HR_prob_dict = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('results/LR_prob_dict.pickle', 'rb') as f:\n",
    "    LR_prob_dict = pickle.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('results/ood_prob_dict.pickle', 'rb') as f:\n",
    "    ood_prob_dict = pickle.load(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Installation year detection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "LR_threshold = 0.97\n",
    "blur_threshold = 0.29\n",
    "ood_threshold = 0.09"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Given LR prediction score, and the info of OOD (blur), return the year of installation.\n",
    "# Using OOD prediction (multiclass: [whether it is ood, whether it is HR]).\n",
    "# In this function, reference list (HR_prob > thres) is assumed to be the key values of LR_prob_dict.\n",
    "def hybrid_model_5(LR_prob_dict, blur_info, LR_threshold=0.5, ood_threshold=0.5, blur_threshold=0.5):\n",
    "    \"\"\"\n",
    "    LR_prob_dict: key1: anchor_filename, key2: target_filename, value: prob produced by LR model\n",
    "    blur_info: key: filename, value: an array of two scores (OOD score and blur score)\n",
    "    LR_threshold: to determine whether a LR image is positive or not.\n",
    "    ood_threshold: to determint whether a image is out-of-distribution (\"impossible to detect\") or not.\n",
    "    blur_threshold: to determine whether a image is HR or LR.\n",
    "    \"\"\" \n",
    "    def is_anchor_candidate(f):\n",
    "        \"\"\" Determine whether an image can be a candidate of the \"positive anchor\" based on its blur \n",
    "        score and OOD score. \"\"\"\n",
    "        if  blur_info[f][1] >= blur_threshold and blur_info[f][0] >= ood_threshold:\n",
    "            return True  # HR\n",
    "        else:\n",
    "            return False  # LR or OOD\n",
    "    \n",
    "    # reference list: a list of image filenames with its HR prediction score >= HR_threshold\n",
    "    reference_list = sorted(LR_prob_dict.keys()) # sorted in the time order\n",
    "    \n",
    "    # determine the \"positive anchor\"\n",
    "    selected_anchors = [f for f in reference_list if is_anchor_candidate(f)]\n",
    "    if selected_anchors:\n",
    "        positive_anchor = selected_anchors[0] # use the earliest anchor image as the \"positive anchor\"\n",
    "    else:\n",
    "        positive_anchor = reference_list[-1]\n",
    "    \n",
    "    # determine the first target (LR) that surpass the threshold based on all referenced anchors\n",
    "    for target in sorted(LR_prob_dict[positive_anchor].keys()): # go through all images\n",
    "        if is_anchor_candidate(target): # skip those images with is HR\n",
    "            continue\n",
    "        if int(target.split('_')[0]) > int(positive_anchor.split('_')[0]): # skip those images later than positive anchor\n",
    "            continue\n",
    "        if blur_info[target][0] < ood_threshold: # don't consider OOD images but record them\n",
    "            continue\n",
    "        for ref in reference_list:\n",
    "            if LR_prob_dict[ref][target] > LR_threshold:\n",
    "                return max(min(2017, int(target.split('_')[0])), 2005), positive_anchor\n",
    "        \n",
    "    return max(min(2017, int(positive_anchor.split('_')[0])), 2005), positive_anchor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# To gather all \"critial\" years that are missing but may change the year prediction.\n",
    "# \"critical\" means that it is RIGHT before the predicted installation year\n",
    "def backtrack_missing_critical_years(LR_prob_dict, \n",
    "                                     blur_info, \n",
    "                                     positive_anchor, \n",
    "                                     installation_year,\n",
    "                                     LR_threshold,\n",
    "                                     ood_threshold,\n",
    "                                     blur_threshold):\n",
    "    \"\"\"\n",
    "    LR_prob_dict: key1: anchor_filename, key2: target_filename, value: prob produced by LR model\n",
    "    blur_info: key: filename, value: an array of two scores (OOD score and blur score)\n",
    "    ood_images: a list of image filenames which are identified as OOD and thus can be regarded as missing\n",
    "    positive_anchor: the anchor image filename which is the earliest HR positive sample\n",
    "    installation_year: the predicted year of installation\n",
    "    LR_threshold: to determine whether a LR image is positive or not\n",
    "    ood_threshold: to determint whether a image is out-of-distribution (\"impossible to detect\") or not.\n",
    "    blur_threshold: to determine whether a image is HR or LR.\n",
    "    \"\"\"\n",
    "    all_images = sorted(LR_prob_dict[positive_anchor].keys()) # all image filenames in that sequence in the time order\n",
    "    \n",
    "    # reference list: a list of image filenames with its HR prediction score >= HR_threshold\n",
    "    reference_list = set(sorted(LR_prob_dict.keys())) # sorted in the time order\n",
    "    \n",
    "    all_downloaded_years = {} # Note: only consider those years no later than installation_year\n",
    "    for f in all_images:\n",
    "        year = int(f.split('_')[0])\n",
    "        if blur_info[f][0] >= ood_threshold or f in reference_list:  # OOD images are regarded as missing\n",
    "            if year not in all_downloaded_years:\n",
    "                all_downloaded_years[year] = []\n",
    "            all_downloaded_years[year].append(f)\n",
    "            \n",
    "    missing_critial_years = []\n",
    "    # backtracking\n",
    "    curr_year = installation_year - 1\n",
    "    while curr_year >= 2005 and curr_year not in all_downloaded_years:\n",
    "        missing_critial_years.append(curr_year)\n",
    "        curr_year -= 1\n",
    "    \n",
    "    if not missing_critial_years:  # no missing\n",
    "        return missing_critial_years\n",
    "    \n",
    "    if installation_year not in all_downloaded_years:  # it indicates that the actual predicted year is 2018 but restricted to 2017\n",
    "        assert installation_year == 2017\n",
    "        return missing_critial_years + [2017]\n",
    "    \n",
    "#     if len(all_downloaded_years[installation_year]) == 1:  # only one image in that year\n",
    "#         return missing_critial_years\n",
    "    \n",
    "    for f in all_downloaded_years[installation_year]:  \n",
    "        # if any one of the images in the installtion year is negative (HR negative and LR negative), \n",
    "        # then we can infer one sample is positive and another is negative in that year, \n",
    "        # thus the solar panel must be installed in that year\n",
    "        # then there is no missing critical year\n",
    "        if blur_info[f][1] >= blur_threshold and f not in reference_list:\n",
    "            return []\n",
    "        if blur_info[f][1] < blur_threshold and f not in reference_list and all([LR_prob_dict[x][f] < LR_threshold for x in reference_list]):\n",
    "            return []\n",
    "    \n",
    "    return missing_critial_years # a list of missing critial years"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "installation_year_dict = {} # sequence idx -> predicted installation year\n",
    "missing_years_dict = {} # sequence idx -> a list of missing critial years\n",
    "for idx in tqdm(HR_prob_dict):\n",
    "    LR_prob_dict_sub = LR_prob_dict[idx]\n",
    "    blur_info = ood_prob_dict[idx]\n",
    "    installation_year, positive_anchor = hybrid_model_5(LR_prob_dict_sub, blur_info, LR_threshold, \n",
    "                                                    ood_threshold, blur_threshold)\n",
    "    missing_years = backtrack_missing_critical_years(LR_prob_dict_sub, blur_info, positive_anchor, \n",
    "                                                     installation_year,\n",
    "                                                     LR_threshold, ood_threshold, blur_threshold)\n",
    "    installation_year_dict[int(idx)] = installation_year\n",
    "    if missing_years:\n",
    "#         if not installation_year in missing_years:\n",
    "#             missing_years_dict[int(idx)] = missing_years + [installation_year]\n",
    "#         else:\n",
    "        missing_years_dict[int(idx)] = missing_years\n",
    "print(len(installation_year_dict))\n",
    "print(len(missing_years_dict))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('results/installation_year_prediction_dict.pickle', 'wb') as f:\n",
    "    pickle.dump(installation_year_dict, f) \n",
    "with open('results/missing_years_dict.pickle', 'wb') as f:\n",
    "    pickle.dump(missing_years_dict, f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Environment (conda_tensorflow_p36)",
   "language": "python",
   "name": "conda_tensorflow_p36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}


# [predict_HR.py](../predict_HR.py)
from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets, models, transforms, utils
import torchvision.transforms.functional as TF

from tqdm import tqdm
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
# import skimage
# import skimage.io
# import skimage.transform
from PIL import Image
import time
import os
from os.path import join, exists
import copy
import random
from collections import OrderedDict
from sklearn.metrics import r2_score

from torch.nn import functional as F
from torchvision.models import Inception3, resnet18, resnet34, resnet50

from utils.image_dataset import *
from LR_models.siamese_model_rgb import *

"""
This script is for generating the prediction scores of HR model for images in sequences.
A sequence of images are stored in a folder. An image is named by the year of the image 
plus an auxiliary index. E.g., '2007_0.png', '2007_1.png', '2008_0.png'.
"""

dir_list = ['demo_sequences']

root_data_dir = 'data/sequences'
old_ckpt_path = 'checkpoint/HR_decay_10_lr_0.0001_8_last.tar'
result_path = 'results/HR_prob_dict.pickle'
error_list_path = 'results/HR_error_list.pickle'

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
input_size = 299
batch_size = 64


class MyCrop:
    def __init__(self, top, left, height, width):
        self.top = top
        self.left = left
        self.height = height
        self.width = width

    def __call__(self, img):
        return TF.crop(img, self.top, self.left, self.height, self.width)


class SingleImageDatasetModified(Dataset):
    def __init__(self, dir_list, transform, latest_prob_dict):
        self.path_list = []
        self.transform = transform

        for subdir in dir_list:
            data_dir = join(root_data_dir, subdir)
            for folder in os.listdir(data_dir):
                idx = folder.split('_')[0]
                folder_dir = join(data_dir, folder)
                for f in os.listdir(folder_dir):
                    if not f[-4:] == '.png':
                        continue
                    if idx in latest_prob_dict and f in latest_prob_dict[idx]:
                        continue
                    self.path_list.append((subdir, folder, f))

    def __len__(self):
        return len(self.path_list)

    def __getitem__(self, index):
        subdir, folder, fname = self.path_list[index]
        image_path = join(root_data_dir, subdir, folder, fname)
        idx = folder.split('_')[0]
        img = Image.open(image_path)
        if not img.mode == 'RGB':
            img = img.convert('RGB')
        img = self.transform(img)
        return img, idx, fname


transform_test = transforms.Compose([
                 transforms.Resize(input_size),
                 transforms.ToTensor(),
                 transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
                 ])


if __name__ == '__main__':
    # load existing prob dict or initialize a new one
    if exists(result_path):
        with open(result_path, 'rb') as f:
            prob_dict = pickle.load(f)
    else:
        prob_dict = {}

    # load existing error list or initialize a new one
    if exists(error_list_path):
        with open(error_list_path, 'rb') as f:
            error_list = pickle.load(f)
    else:
        error_list = []

    # dataloader
    dataset_pred = SingleImageDatasetModified(dir_list, transform=transform_test, latest_prob_dict=prob_dict)
    print('Dataset size: ' + str(len(dataset_pred)))
    dataloader_pred = DataLoader(dataset_pred, batch_size=batch_size, shuffle=False, num_workers=4)

    # model
    model = Inception3(num_classes=2, aux_logits=True, transform_input=False)
    model = model.to(device)

    # load old parameters
    checkpoint = torch.load(old_ckpt_path, map_location=device)
    if old_ckpt_path[-4:] == '.tar':  # it is a checkpoint dictionary rather than just model parameters
        model.load_state_dict(checkpoint['model_state_dict'])
    else:
        model.load_state_dict(checkpoint)
    print('Old checkpoint loaded: ' + old_ckpt_path)

    model.eval()
    # run
    count = 0
    for inputs, idx_list, fname_list in tqdm(dataloader_pred):
        try:
            inputs = inputs.to(device)
            with torch.set_grad_enabled(False):
                outputs = model(inputs)
                prob = F.softmax(outputs, dim=1)
            pos_prob_list = prob[:, 1].cpu().numpy()
            for i in range(len(idx_list)):
                idx = idx_list[i]
                fname = fname_list[i]
                pos_prob = pos_prob_list[i]

                if not idx in prob_dict:
                    prob_dict[idx] = {}
                prob_dict[idx][fname] = pos_prob

        except:  # take a note on the batch that causes error
            error_list.append((idx_list, fname_list))
        if count % 200 == 0:
            with open(join(result_path), 'wb') as f:
                pickle.dump(prob_dict, f)
            with open(join(error_list_path), 'wb') as f:
                pickle.dump(error_list, f)
        count += 1

    with open(join(result_path), 'wb') as f:
        pickle.dump(prob_dict, f)
    with open(join(error_list_path), 'wb') as f:
        pickle.dump(error_list, f)

    print('Done!')

# ~/LICENSE.md

                                 Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

# [hp_search_ood_multilabels.py](../hp_search_ood_multilabels.py)


from __future__ import print_function
from __future__ import division
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import datasets, models, transforms, utils
import torchvision.transforms.functional as TF

from tqdm import tqdm
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
# import skimage
# import skimage.io
# import skimage.transform
from PIL import Image
import time
import os
from os.path import join, exists
import copy
import random
from collections import OrderedDict
from sklearn.metrics import r2_score

from torch.nn import functional as F
from torchvision.models import Inception3, resnet18, resnet34, resnet50

from utils.image_dataset import *
from LR_models.siamese_model_rgb import *

"""
This script is for training blur detection model that classifies an image
into three classes: OOD (out of distribution, extremely blurred), HR (high
resolution), LR (low resolution). It is a single-branch CNN based on 
ResNet-50 model. The hyperparameters to search include learning rate, 
learning rate decay epochs, and weight decay.
"""

# Configuration
# directory for loading training/validation/test data
# for each of "train"/"val"/"test", put the image folders of class "OOD" to the first 
# list, put the image folders of class "LR" to the second list, and put image folders 
# of class "HR" to the third list.

dirs_list_dict = {
    'train':
    [[
        'data/blur_detection_images/train/OOD',
    ],
     [
        'data/blur_detection_images/train/LR',
    ],
     [
        'data/blur_detection_images/train/HR',
    ]],
    'val':
    [[
        'data/blur_detection_images/val/OOD',
    ],
     [
        'data/blur_detection_images/val/LR',
    ],
     [
        'data/blur_detection_images/val/HR',
    ]],
    'test':
    [[
        'data/blur_detection_images/test/OOD',
    ],
     [
        'data/blur_detection_images/test/LR',
    ],
     [
        'data/blur_detection_images/test/HR',
    ]],
}

old_ckpt_path_dict = {
    'resnet34': 'checkpoint/resnet34-333f7ec4.pth',
    'resnet50': 'checkpoint/resnet50-19c8e357.pth',
}
# directory for saving model/checkpoint
ckpt_save_dir = 'checkpoint/ood_new_model'

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model_arch = 'resnet50'
nclasses = 2
old_ckpt_path = old_ckpt_path_dict[model_arch] # path to load old model/checkpoint, set it to None if not loading
trainable_params = None     # layers or modules set to be trainable. "None" if training all layers
model_name = 'ood'          # the prefix of the filename for saving model/checkpoint
return_best = True           # whether to return the best model according to the validation metrics
if_early_stop = True         # whether to stop early after validation metrics doesn't improve for definite number of epochs
input_size = 299              # image size fed into the model
imbalance_rate = 1.0            # weight given to the positive (rarer) samples in loss function
learning_rate = 0.0001         # learning rate
# weight_decay = 0           # l2 regularization coefficient
batch_size = 64
num_epochs = 100              # number of epochs to train
lr_decay_rate = 0.95           # learning rate decay rate for each decay step
# lr_decay_epochs = 10          # number of epochs for one learning rate decay
early_stop_epochs = 10        # after validation metrics doesn't improve for "early_stop_epochs" epochs, stop the training.
save_epochs = 50              # save the model/checkpoint every "save_epochs" epochs
# threshold = 0.2               # threshold probability to identify am image as positive
ib1 = 1 # weight for imbalance class 

# hyperparamters to tune
lr_list = [0.00001, 0.0001, 0.001] # learning rates
lr_decay_epochs_list = [10, 4] # learning rate decay epochs
weight_decay_list = [0, 0.001] # weight decay
threshold_list = np.linspace(0.0, 1.0, 101).tolist()


def RandomRotationNew(image):
    angle = random.choice([0, 180])
    image = TF.rotate(image, angle)
    return image


class MyCrop:
    def __init__(self, top, left, height, width):
        self.top = top
        self.left = left
        self.height = height
        self.width = width

    def __call__(self, img):
        return TF.crop(img, self.top, self.left, self.height, self.width)


def only_train(model, trainable_params):
    """trainable_params: The list of parameters and modules that are set to be trainable.
    Set require_grad = False for all those parameters not in the trainable_params"""
    print('Only the following layers:')
    for name, p in model.named_parameters():
        p.requires_grad = False
        for target in trainable_params:
            if target == name or target in name:
                p.requires_grad = True
                print('    ' + name)
                break


def metrics(stats):
    """stats: {'TP': TP, 'FP': FP, 'TN': TN, 'FN': FN}
    return: must be a single number """
    precision = (stats['TP'] + 0.00001) * 1.0 / (stats['TP'] + stats['FP'] + 0.00001)
    recall = (stats['TP'] + 0.00001) * 1.0 / (stats['TP'] + stats['FN'] + 0.00001)
    spec = (stats['TN'] + 0.00001) * 1.0 / (stats['TN'] + stats['FP'] + 0.00001)
    sens = (stats['TP'] + 0.00001) * 1.0 / (stats['TP'] + stats['FN'] + 0.00001)
    hm1 = 2.0 * precision * recall / (precision + recall + 1e-7)
    hm2 = 2.0 * spec * sens / (spec + sens + 1e-7)
    return hm1, hm2


def train_model(model, model_name, dataloaders, criterion, optimizer, metrics, num_epochs, training_log=None,
                verbose=True, return_best=True, if_early_stop=True, early_stop_epochs=10, scheduler=None,
                save_dir=None, save_epochs=5):
    since = time.time()
    if not training_log:
        training_log = dict()
        training_log['train_loss_history'] = []
        training_log['val_loss_history'] = []
        training_log['val_metric_value_history'] = []
        training_log['epoch_best_threshold_history'] = []
        training_log['current_epoch'] = -1
    current_epoch = training_log['current_epoch'] + 1

    best_model_wts = copy.deepcopy(model.state_dict())
    best_optimizer_wts = copy.deepcopy(optimizer.state_dict())
    best_log = copy.deepcopy(training_log)

    best_metric_value = -np.inf
    best_threshold_1 = 0
    best_threshold_2 = 0
    nodecrease = 0  # to count the epochs that val loss doesn't decrease
    early_stop = False

    for epoch in range(current_epoch, current_epoch + num_epochs):
        if verbose:
            print('Epoch {}/{}'.format(epoch, num_epochs - 1))
            print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0

            stats1 = {x: {'TP': 0, 'FP': 0, 'TN': 0, 'FN': 0} for x in threshold_list}
            stats2 = {x: {'TP': 0, 'FP': 0, 'TN': 0, 'FN': 0} for x in threshold_list}

            # Iterate over data.
            for inputs, labels in tqdm(dataloaders[phase]):
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    if phase == 'train':
                        if model_arch != 'inception':
                            outputs = model(inputs)
                            loss = criterion(outputs, labels)
                        else:
                            outputs, aux_outputs = model(inputs)
                            loss1 = criterion(outputs, labels)
                            loss2 = criterion(aux_outputs, labels)
                            loss = loss1 + 0.4 * loss2

                        # backward + optimize only if in training phase
                        loss.backward()
                        optimizer.step()

                    else:
                        outputs = model(inputs)
                        loss = criterion(outputs, labels)
                        # val phase: calculate metrics under different threshold
                        prob = torch.sigmoid(outputs)

                        labels1 = labels[:, 0]
                        labels2 = labels[:, 1]

                        for threshold1 in threshold_list:
                            preds1 = prob[:, 0] >= threshold1
                            stats1[threshold1]['TP'] += torch.sum((preds1 == 1) * (labels1 == 1)).cpu().item()
                            stats1[threshold1]['TN'] += torch.sum((preds1 == 0) * (labels1 == 0)).cpu().item()
                            stats1[threshold1]['FP'] += torch.sum((preds1 == 1) * (labels1 == 0)).cpu().item()
                            stats1[threshold1]['FN'] += torch.sum((preds1 == 0) * (labels1 == 1)).cpu().item()

                        for threshold2 in threshold_list:
                            preds2 = prob[:, 1] >= threshold2
                            stats2[threshold2]['TP'] += torch.sum((preds2 == 1) * (labels2 == 1)).cpu().item()
                            stats2[threshold2]['TN'] += torch.sum((preds2 == 0) * (labels2 == 0)).cpu().item()
                            stats2[threshold2]['FP'] += torch.sum((preds2 == 1) * (labels2 == 0)).cpu().item()
                            stats2[threshold2]['FN'] += torch.sum((preds2 == 0) * (labels2 == 1)).cpu().item()

                # loss accumulation
                running_loss += loss.item() * inputs.size(0)

            training_log['current_epoch'] = epoch
            epoch_loss = running_loss / len(dataloaders[phase].dataset)

            if phase == 'train':
                training_log['train_loss_history'].append(epoch_loss)
                if scheduler is not None:
                    scheduler.step()
                if verbose:
                    print('{} Loss: {:.4f}'.format(phase, epoch_loss))

            if phase == 'val':
                epoch_best_threshold_1 = 0.0
                epoch_best_threshold_2 = 0.0
                epoch_max_metrics_1 = 0.0
                epoch_max_metrics_2 = 0.0
                for threshold1 in threshold_list:
                    metric_value_1, _ = metrics(stats1[threshold1])
                    if metric_value_1 > epoch_max_metrics_1:
                        epoch_best_threshold_1 = threshold1
                        epoch_max_metrics_1 = metric_value_1

                for threshold2 in threshold_list:
                    _, metric_value_2 = metrics(stats2[threshold2])
                    if metric_value_2 > epoch_max_metrics_2:
                        epoch_best_threshold_2 = threshold2
                        epoch_max_metrics_2 = metric_value_2

                epoch_max_metrics = 2.0 * epoch_max_metrics_1 * (epoch_max_metrics_2**2) / (epoch_max_metrics_1 + (epoch_max_metrics_2**2))

                recall = (stats1[epoch_best_threshold_1]['TP'] + 0.00001) * 1.0 / (
                    stats1[epoch_best_threshold_1]['TP'] + stats1[epoch_best_threshold_1]['FN'] + 0.00001)
                precision = (stats1[epoch_best_threshold_1]['TP'] + 0.00001) * 1.0 / (
                    stats1[epoch_best_threshold_1]['TP'] + stats1[epoch_best_threshold_1]['FP'] + 0.00001)

                spec = (stats2[epoch_best_threshold_2]['TN'] + 0.00001) * 1.0 / (
                    stats2[epoch_best_threshold_2]['TN'] + stats2[epoch_best_threshold_2]['FP'] + 0.00001)
                sens = (stats2[epoch_best_threshold_2]['TP'] + 0.00001) * 1.0 / (
                    stats2[epoch_best_threshold_2]['TP'] + stats2[epoch_best_threshold_2]['FN'] + 0.00001)

                if verbose:
                    print('{} Loss: {:.4f} Metrics: {:.4f} Threshold1: {:.4f} Threshold2: {:.4f} Recall: {:.4f} Precision: {:.4f} Sensitivity: {:.4f} Specificity: {:.4f}'.format(phase, epoch_loss,
                          epoch_max_metrics, epoch_best_threshold_1, epoch_best_threshold_2, recall, precision, sens, spec))

                training_log['val_metric_value_history'].append(epoch_max_metrics)
                training_log['val_loss_history'].append(epoch_loss)
                training_log['epoch_best_threshold_history'].append([epoch_best_threshold_1, epoch_best_threshold_2])

                # deep copy the model
                if epoch_max_metrics > best_metric_value:
                    best_metric_value = epoch_max_metrics
                    best_threshold_1 = epoch_best_threshold_1
                    best_threshold_2 = epoch_best_threshold_2
                    best_model_wts = copy.deepcopy(model.state_dict())
                    best_optimizer_wts = copy.deepcopy(optimizer.state_dict())
                    best_log = copy.deepcopy(training_log)
                    nodecrease = 0
                else:
                    nodecrease += 1

            if nodecrease >= early_stop_epochs:
                early_stop = True

        if save_dir and epoch % save_epochs == 0 and epoch > 0:
            checkpoint = {
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'training_log': training_log
            }
            torch.save(checkpoint,
                       os.path.join(save_dir, model_name + '_' + str(training_log['current_epoch']) + '.tar'))

        if if_early_stop and early_stop:
            print('Early stopped!')
            break

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best validation metric value: {:4f}'.format(best_metric_value))
    # print('Best validation threshold 1: {:4f}'.format(best_threshold_1))

    # load best model weights
    if return_best:
        model.load_state_dict(best_model_wts)
        optimizer.load_state_dict(best_optimizer_wts)
        training_log = best_log

    checkpoint = {
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'training_log': training_log
    }
    torch.save(checkpoint,
               os.path.join(save_dir, model_name + '_' + str(training_log['current_epoch']) + '_last.tar'))

    return model, training_log, best_metric_value, best_threshold_1, best_threshold_2


def test_model(model, dataloader, metrics, threshold_list):
    stats1 = {x: {'TP': 0, 'FP': 0, 'TN': 0, 'FN': 0} for x in threshold_list}
    stats2 = {x: {'TP': 0, 'FP': 0, 'TN': 0, 'FN': 0} for x in threshold_list}

    metric_values = {}
    metric_values_1 = {}
    metric_values_2 = {}

    model.eval()
    for inputs, labels in tqdm(dataloader):
        inputs = inputs.to(device)
        labels = labels.to(device)

        with torch.set_grad_enabled(False):
            outputs = model(inputs)
            prob = torch.sigmoid(outputs)

            labels1 = labels[:, 0]
            labels2 = labels[:, 1]

            for threshold1 in threshold_list:
                preds1 = prob[:, 0] >= threshold1
                stats1[threshold1]['TP'] += torch.sum((preds1 == 1) * (labels1 == 1)).cpu().item()
                stats1[threshold1]['TN'] += torch.sum((preds1 == 0) * (labels1 == 0)).cpu().item()
                stats1[threshold1]['FP'] += torch.sum((preds1 == 1) * (labels1 == 0)).cpu().item()
                stats1[threshold1]['FN'] += torch.sum((preds1 == 0) * (labels1 == 1)).cpu().item()

            for threshold2 in threshold_list:
                preds2 = prob[:, 1] >= threshold2
                stats2[threshold2]['TP'] += torch.sum((preds2 == 1) * (labels2 == 1)).cpu().item()
                stats2[threshold2]['TN'] += torch.sum((preds2 == 0) * (labels2 == 0)).cpu().item()
                stats2[threshold2]['FP'] += torch.sum((preds2 == 1) * (labels2 == 0)).cpu().item()
                stats2[threshold2]['FN'] += torch.sum((preds2 == 0) * (labels2 == 1)).cpu().item()

    for threshold1 in threshold_list:
        for threshold2 in threshold_list:
            metric_value_1, _ = metrics(stats1[threshold1])
            _, metric_value_2 = metrics(stats2[threshold2])
            metric_values[(threshold1, threshold2)] = 2.0 * metric_value_1 * (metric_value_2**2) / (metric_value_1 + (metric_value_2**2) + 1e-8)
            metric_values_1[threshold1] = metric_value_1
            metric_values_2[threshold2] = metric_value_2
    return metric_values, metric_values_1, metric_values_2


data_transforms = {
    'train': transforms.Compose([
        transforms.Resize((input_size, input_size)),
        MyCrop(17, 0, 240, 299),
        transforms.Lambda(RandomRotationNew),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.RandomVerticalFlip(p=0.5),
        # transforms.Resize((input_size, input_size)),
        transforms.ToTensor(),
        # transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ]),
    'val': transforms.Compose([
        transforms.Resize((input_size, input_size)),
        MyCrop(17, 0, 240, 299),
        # transforms.Resize((input_size, input_size)),
        transforms.ToTensor(),
        # transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ]),
    'test': transforms.Compose([
        transforms.Resize((input_size, input_size)),
        MyCrop(17, 0, 240, 299),
        # transforms.Resize((input_size, input_size)),
        transforms.ToTensor(),
        # transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    ])
}

if __name__ == '__main__':
    # data
    image_datasets = {x: FolderDirsDatasetMultiLabels(dirs_list_dict[x], transform=data_transforms[x]) for x in ['train', 'val', 'test']}
    dataloaders_dict = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size,
                                                       shuffle=True, num_workers=4) for x in ['train', 'val', 'test']}

    print('Training set size: ' + str(len(image_datasets['train'])))
    print('Validation set size: ' + str(len(image_datasets['val'])))
    print('Test set size: ' + str(len(image_datasets['test'])))

    results_dict = {x: {y: {z: {} for z in weight_decay_list} for y in lr_decay_epochs_list} for x in lr_list}

    if not os.path.exists(ckpt_save_dir):
        os.mkdir(ckpt_save_dir)

    # model
    for learning_rate in lr_list:
        for lr_decay_epochs in lr_decay_epochs_list:
            for weight_decay in weight_decay_list:
                print('----------------------- ' +
                      str(learning_rate) + ', ' +
                      str(lr_decay_epochs) + ', ' +
                      str(weight_decay) +
                      ' -----------------------')
                if model_arch == 'resnet18':
                    model = resnet18(num_classes=nclasses)
                elif model_arch == 'resnet34':
                    model = resnet34(num_classes=nclasses)
                elif model_arch == 'resnet50':
                    model = resnet50(num_classes=nclasses)
                elif model_arch == 'inception':
                    model = Inception3(num_classes=nclasses, aux_logits=True, transform_input=False)
                else:
                    raise
                optimizer = optim.Adam(model.parameters(), lr=learning_rate, betas=(0.9, 0.999), eps=1e-08,
                                       weight_decay=weight_decay, amsgrad=True)
                pos_weight = torch.tensor([ib1, 1], dtype=torch.float).cuda()
                loss_fn = nn.BCEWithLogitsLoss(pos_weight=pos_weight)
                scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=lr_decay_epochs, gamma=lr_decay_rate)

                # load old parameters
                if old_ckpt_path:
                    checkpoint = torch.load(old_ckpt_path)
                    if old_ckpt_path[-4:] == '.tar':  # it is a checkpoint dictionary rather than just model parameters
                        model.load_state_dict(checkpoint['model_state_dict'])
                        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
                        training_log = checkpoint['training_log']

                    else:
                        del checkpoint['fc.weight']
                        del checkpoint['fc.bias']
                        if model_arch == 'inception':
                            del checkpoint['AuxLogits.fc.weight']
                            del checkpoint['AuxLogits.fc.bias']
                        model.load_state_dict(checkpoint, strict=False)
                        training_log = None  # start from scratch

                    print('Old checkpoint loaded: ' + old_ckpt_path)

                model = model.to(device)

                # fix some layers and make others trainable
                if trainable_params:
                    only_train(model, trainable_params)

                best_model, _, best_metric_value, best_threshold_1, best_threshold_2 = train_model(model, model_name=model_name + '_lr_' + str(
                    learning_rate) + '_decay_' + str(lr_decay_epochs) + '_wd_' + str(weight_decay),
                                                                      dataloaders=dataloaders_dict, criterion=loss_fn,
                                                                      optimizer=optimizer, metrics=metrics,
                                                                      num_epochs=num_epochs,
                                                                      training_log=training_log, verbose=True,
                                                                      return_best=return_best,
                                                                      if_early_stop=if_early_stop,
                                                                      early_stop_epochs=early_stop_epochs,
                                                                      scheduler=scheduler, save_dir=ckpt_save_dir,
                                                                      save_epochs=save_epochs)

                print('Begin test ...')
                test_metric_values, metric_values_1, metric_values_2 = test_model(best_model, dataloaders_dict['test'], metrics, threshold_list=threshold_list)

                best_threshold_test, best_metric_value_test = \
                sorted(list(test_metric_values.items()), key=lambda tup: tup[1], reverse=True)[0]

                results_dict[learning_rate][lr_decay_epochs][weight_decay] = {'best_metrics_val': best_metric_value,
                                                                    'best_threshold_val': (best_threshold_1, best_threshold_2),
                                                                    'best_metric_test': best_metric_value_test,
                                                                    'best_threshold_test': best_threshold_test,
                                                                    'test_metrics_with_val_best_threshold':
                                                                        test_metric_values[(best_threshold_1, best_threshold_2)],
                                                                    'test_metrics_1_with_val_best_threshold':
                                                                        metric_values_1[best_threshold_1],
                                                                    'test_metrics_2_with_val_best_threshold':
                                                                        metric_values_2[best_threshold_2]
                                                                 }

                with open(join(ckpt_save_dir, 'results_dict.pickle'), 'wb') as f:
                    pickle.dump(results_dict, f)

    print(results_dict)


